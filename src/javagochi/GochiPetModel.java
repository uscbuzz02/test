/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javagochi;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class GochiPetModel {
    private int HP;
    private int SP;
    private int EP;
    private int lvl;
    private String name;
    private int petType;
    private BufferedImage petImg;
    
    private FileWriter fw;
    
    public GochiPetModel(String petFile){
        Path file = Paths.get("./"+petFile);
        
        
        this.EP = 20;
        this.SP = 30;
        this.HP = 50;
        this.lvl = 0;
        this.name = petFile.substring(5);
        
        initializeModel(file);
    }

    private boolean initializeModel(Path file){
        boolean flagOK = true;
        String line = null;
        
        Charset charset = Charset.forName("UTF-8");
        try (BufferedReader reader = Files.newBufferedReader(file, charset)) {
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException x) {
            //System.err.format("READ IOException: %s%n", x);
            flagOK = false;
            
            if(x.getClass().getName().equals("java.nio.file.NoSuchFileException")){
                //FILE NOT FOUND, then CREATE FILE
                FileWriter testfile;
                try {
                    testfile = new FileWriter("sample/sample.txt", true);
                    testfile.write("HP:"+this.HP+"|SP:"+this.SP+"|EP:"+this.EP+"|lvl:"+this.lvl+"|name:"+this.name);
                    testfile.close();
                } catch (IOException ex) {
                    Logger.getLogger(GochiPetModel.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                flagOK = createFile(file);
            }
        }
        System.out.println(line);
        return flagOK;
    }
    
    private boolean createFile(Path file){
        boolean flagOK = true;
        
        Charset charset = Charset.forName("UTF-8");
        String s = "HP:"+this.HP+"|SP:"+this.SP+"|EP:"+this.EP+"|lvl:"+this.lvl+"|name:"+this.name;
        try (BufferedWriter writer = Files.newBufferedWriter(file, charset)) {
            writer.write(s, 0, s.length());
        } catch (IOException x) {
            System.err.format("CREATE IOException: %s%n", x);
            flagOK = false;
        }
        
        return flagOK;
    }
    /**
     * @return the HP
     */
    public int getHP() {
        return HP;
    }

    /**
     * @param HP the HP to set
     */
    public void setHP(int HP) {
        this.HP = HP;
    }

    /**
     * @return the SP
     */
    public int getSP() {
        return SP;
    }

    /**
     * @param SP the SP to set
     */
    public void setSP(int SP) {
        this.SP = SP;
    }

    /**
     * @return the EP
     */
    public int getEP() {
        return EP;
    }

    /**
     * @param EP the EP to set
     */
    public void setEP(int EP) {
        this.EP = EP;
    }

    /**
     * @return the lvl
     */
    public int getLvl() {
        return lvl;
    }

    /**
     * @param lvl the lvl to set
     */
    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the petType
     */
    public int getPetType() {
        return petType;
    }

    /**
     * @param petType the petType to set
     */
    public void setPetType(int petType) {
        this.petType = petType;
    }

    /**
     * @return the petImg
     */
    public BufferedImage getPetImg() {
        return petImg;
    }

    /**
     * @param petImg the petImg to set
     */
    public void setPetImg(BufferedImage petImg) {
        this.petImg = petImg;
    }
    
}
